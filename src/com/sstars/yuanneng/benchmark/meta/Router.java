/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sstars.yuanneng.benchmark.meta;

import com.google.protobuf.Message;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @author madk
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Router {
    //消息类型 A01
    public String type();
    //消息编号，-6
    public int flag() default 1;
    //protobuf的消息类型
    public Class<? extends Message> msgType() default Message.class;
}
