/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sstars.yuanneng.benchmark.robot;

import com.google.protobuf.Message;
import com.sstars.yuanneng.benchmark.Main;
import com.sstars.yuanneng.benchmark.Setting;
import com.sstars.yuanneng.benchmark.meta.Router;
import com.sstars.yuanneng.benchmark.robot.features.Chat;
import com.sstars.yuanneng.benchmark.robot.features.Feature;
import com.sstars.yuanneng.benchmark.robot.features.Login;
import com.sstars.yuanneng.benchmark.robot.features.Room;
import com.sstars.yuanneng.benchmark.utils.ArrayUtils;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 机器人类
 * @author madk
 */
public class Robot{
    
    /** 消息类型到protobuf数据包类型的映射表 **/
    private static Map<String, Class<? extends Message>> pbTypeTbl = new HashMap<String, Class<? extends Message>>();
    /** 消息类型到处理函数的映射表 **/
    private static Map<String, IdxMethodPair> dealerTbl = new HashMap<String, IdxMethodPair>();
    
    private static int totalEnteredNum = 0;
    /** 功能处理对象的类
     在Robot类初始化(系统启动)时，遍历所有的feature类，构造消息头-protobuf类型和处理函数的映射表
     **/
    private static Class[] featureClasses = new Class[] {Login.class,Room.class,Chat.class };
    {
        for(int i = 0; i < featureClasses.length; ++i){
            Class featureCls = featureClasses[i];
            for(Method method : featureCls.getDeclaredMethods()){
                Router router = method.getAnnotation(Router.class);
                if(router == null){
                    continue;
                }
                String msgType = String.format("%s%d", router.type(),router.flag());
                pbTypeTbl.put(msgType, router.msgType());
                dealerTbl.put(msgType, new IdxMethodPair(i, method));
            }
        }
    }
    
    /** 功能处理对象的数组 **/
    private Feature[] features = new Feature[featureClasses.length];
    
    //机器人的序号
    private int num;
    //在游戏中的id
    private int gid;
    //登录功能处理
    private Login login;
    //地图功能处理
    private Room room;
    //聊天
    private Chat chat;
    //是否进入了游戏(进入了地图)
    private boolean enteredGame = false;
    /** 上次活动的时间戳 **/
    private long lastActiveTs ;
    
    private SocketChannel chanel;
    private byte[] prevBytes;
    private SelectionKey key;
    public Robot(int num) {
        this.num = num;
        initFeatures();
        try{ 
            chanel = SocketChannel.open();
            Main.registerRobot(chanel, this);
            chanel.connect(new InetSocketAddress(Setting.getHost(), Setting.getPort()));
            chanel.configureBlocking(false);
            key = chanel.register(Main.selector, SelectionKey.OP_READ);
            login.login();
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
    
    public final void initFeatures() {
        for(int i = 0; i < featureClasses.length; ++i){
            try {
                features[i] = (Feature)featureClasses[i].getConstructor(Robot.class).newInstance(this);
                if(featureClasses[i] == Login.class){
                    login = (Login)features[i];
                }else if(featureClasses[i] == Room.class){
                    room = (Room)features[i];
                }else if(featureClasses[i] == Chat.class){
                    chat = (Chat)features[i];
                }
            } catch (NoSuchMethodException ex) {
                Logger.getLogger(Robot.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SecurityException ex) {
                Logger.getLogger(Robot.class.getName()).log(Level.SEVERE, null, ex);
            }catch(InstantiationException ex){
                
            }catch(IllegalAccessException ex){
                
            }catch(InvocationTargetException ex){
                
            }
        }
    }
    
    public static void newEntered() {
        System.out.format("now there are %d robots entered map\n", ++totalEnteredNum);
    }
    /**
     * 获取该机器人在游戏中的唯一id
     * @return 
     */
    public int getGid(){
        return gid;
    }
    
    public void connected() {
        login.login();
    }
    
    /**
     * 机器人进入了游戏
     */
    public void enteredGame(int gid,int map,int x, int y){
        this.gid = gid;
        enteredGame = true;
        room.setMapPoint(map, x, y);
    }
    
    /**
     * 查询机器人是否进入了游戏
     * @return 
     */
    public boolean isEnteredGame() {
        return enteredGame;
    }
    
    /**
     * 向服务器发送不包含protobuf消息体的命令
     * @param cmd 
     */
    public void sendMsgToServer(String cmd){
        sendMsgToServer(cmd,null);
    }
    
    /**
     * 向服务器发送消息
     * @param cmd
     * @param msg 
     */
    public void sendMsgToServer(String cmd,Message msg) {
        try{
            short len = 0;
            byte[] pbBytes = null;
            if(msg != null) {
                pbBytes = msg.toByteArray();
                len = (short)pbBytes.length;
            }
            ByteBuffer bb = ByteBuffer.allocate(4 + len);
            bb.order(ByteOrder.LITTLE_ENDIAN);
            // 2字节长度
            bb.putShort(len);
            // 1字节消息类型
            bb.put((byte)cmd.charAt(0));
            // 1字节消息id
            bb.put((byte)(Integer.parseInt(cmd.substring(1))));
            if(pbBytes != null){
                //protobuf 包
                bb.put(pbBytes);
            }
            bb.flip();
            chanel.write(bb);
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
    
    /**
     * 找点事做
     */
    public void doSomeThing(){
        long nowTs = new Date().getTime();
        if(lastActiveTs >= nowTs - 200) {
            return;
        }
        lastActiveTs = nowTs;
        try{
            room.walk();
            if(Setting.isOpenChat()) {
                chat.saySomeThing();
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
    
    public void readMsg(){
        try{
            ByteBuffer buffer = ByteBuffer.allocate(10240);
            buffer.order(ByteOrder.LITTLE_ENDIAN);
            int len = chanel.read(buffer);
            buffer.position(len);
            buffer.flip();
            byte[] bytes;
            if(prevBytes != null){
                bytes = ArrayUtils.concat(prevBytes, extractRemainBytes(buffer));
                len += prevBytes.length;
                prevBytes = null;
            }else {
                bytes = extractRemainBytes(buffer);
            }
            buffer = ByteBuffer.wrap(bytes);
            buffer.order(ByteOrder.LITTLE_ENDIAN);
//            System.out.format("recved packet, len:%d\n", len);
            
            while(true) {
//                System.out.format("remaining %d\n", buffer.remaining());
                if(buffer.remaining() < 5){
                    if(buffer.remaining() > 0){
                        prevBytes = extractRemainBytes(buffer);
//                        System.out.println("has prev bytes1 " + prevBytes.length);
                    }
                    break;
                }
                int pbLen = buffer.getShort();
                if(buffer.remaining() < pbLen + 3){
                    buffer.position(buffer.position() - 2);
                    prevBytes = extractRemainBytes(buffer);
//                    System.out.format("has prev bytes2 prevBytes len: %d, pblen:%d \n" ,prevBytes.length,pbLen);
                    break;
                }
                char type = (char)buffer.get();
                byte msgId = buffer.get();
                
                int flag = buffer.get();
                String msgType = String.format("%s%02d%d", type,msgId,flag);
                System.out.format("recv msg:%s\n", msgType);
                Message pbPacket = null;
                if(pbLen > 0){
                    byte[] pbBytes = new byte[pbLen];
                    buffer.get(pbBytes);
                    //根据注册的类型，导出pb的数据包
                    if(pbTypeTbl.containsKey(msgType)){
                        Class<? extends Message> cls = pbTypeTbl.get(msgType);
                        Method parseFrom = cls.getMethod("parseFrom", byte[].class);
                        pbPacket = (Message)parseFrom.invoke(null, pbBytes);
                    }
                }
//                System.out.format("recv %s\n", msgType);
                if(dealerTbl.containsKey(msgType)){
                    IdxMethodPair pair = dealerTbl.get(msgType);
                    pair.method.invoke(features[pair.idx], pbPacket);
                }
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
    
    private byte[] extractRemainBytes(ByteBuffer buffer) {
        byte[] bytes = new byte[buffer.remaining()];
        int i = 0;
        while(buffer.remaining() > 0) {
            bytes[i++] = buffer.get();
        }
        return bytes;
    }
    
    /**
     * 连接另外一个端口（进程）
     * @param ip   要连接的服务器的ip
     * @param port 要连接的服务器的端口
     * @param req  A04后附带的消息体
     */
    public void connectOtherPort(String ip,int port){
        try {
            enteredGame = false;
            key.cancel();
            chanel.socket().close();
            Main.unregisterRobot(chanel);
            chanel = SocketChannel.open();
            Main.registerRobot(chanel, this);
            chanel.connect(new InetSocketAddress(ip, port));
            chanel.configureBlocking(false);
            key = chanel.register(Main.selector, SelectionKey.OP_READ);
            login.login();
        } catch (UnknownHostException ex) {
            Logger.getLogger(Robot.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Robot.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int getNum() {
        return num;
    }
}

class IdxMethodPair {
    int idx;
    Method method;

    public IdxMethodPair( int idx, Method method) {
        this.idx = idx;
        this.method = method;
    }
    
}
