/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sstars.yuanneng.benchmark.robot.features;

import com.sstars.yuanneng.benchmark.robot.Robot;

/**
 *
 * @author madk
 */
public abstract class Feature {
    protected Robot robot;
    
    public Feature(Robot robot){
        this.robot = robot;
    }
}
