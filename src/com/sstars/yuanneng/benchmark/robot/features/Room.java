/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sstars.yuanneng.benchmark.robot.features;

import com.sstars.yuanneng.benchmark.meta.Router;
import com.sstars.yuanneng.benchmark.robot.Robot;
import java.awt.Point;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import proto.map.Map.MapBroadRes;
import proto.map.Map.MovePathReq;
import proto.map.Map.PBPoint;
import proto.map.Map.StopMoveRes;

/**
 * 地图和行走等功能
 * @author madk
 */
public class Room extends Feature {

    public static final Random random = new Random(new Date().getTime());
    private Queue<Point> pathQ = new ArrayDeque<Point>();
    private static Map<Integer,Integer[][]> mapPointTbl = new HashMap<Integer,Integer[][]> ();
    {
        for(int mapId : new int[] {70001}){
            BufferedReader reader = new BufferedReader(new InputStreamReader(Room.class.getResourceAsStream("room70001.txt")));
            String line;
            List<Integer[]> mapList = new ArrayList<Integer[]>();
            try {
                while((line = reader.readLine()) != null){
                    if(line.isEmpty()){
                        continue;
                    }
                    String[] items = line.split(" ");
                    Integer[] rowPts = new Integer[items.length];
                    for(int i = 0; i < items.length; i++){
                        rowPts[i] = Integer.parseInt(items[i]);
                    }
                    mapList.add(rowPts);
                }
                mapPointTbl.put(mapId,mapList.toArray(new Integer[][]{}));
            } catch (IOException ex) {
                Logger.getLogger(Room.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                reader.close();
            } catch (IOException ex) {
                Logger.getLogger(Room.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    // 当前地图和坐标
    private int map,x,y;
    public Room(Robot robot){
        super(robot);
    }
    
//    @Router(type = "C01", flag = 1, msgType=MapBroadRes.class)
//    public void enteredRoom(MapBroadRes msg){
//        
//    }
    
    public void setMapPoint(int map, int x, int y){
        this.map = map;
        this.x = x;
        this.y = y;
    }
    
    /**
     * 随机走路
     */
    public void walk(){
        if(pathQ.isEmpty()) {
            //八分之一的情况随机走动
            if(Math.abs(random.nextInt()) % 16 == 0){
                int currentX = x,currentY = y;
                int step = random.nextInt(20);
                findPath: while(step-- > 0){
                    switch(random.nextInt(4)){
                        case 0:
                            if(walkable(currentX + 1, currentY)){
                                ++currentX;
                                break;
                            }
                        case 1:
                            if(walkable(currentX - 1, currentY)){
                                --currentX;
                                break;
                            }
                        case 2:
                            if(walkable(currentX, currentY + 1)){
                                ++currentY;
                                break;
                            }
                            
                        case 3:
                            if(walkable(currentX, currentY - 1)){
                                --currentY;
                                break;
                            }
                        default:
                            //四周都没找到合适的点，退出寻路
                            break findPath;
                    }
                    pathQ.add(new Point(currentX, currentY));
                }
                if(!pathQ.isEmpty()){
                    MovePathReq.Builder req = MovePathReq.newBuilder();
                    for(Point p : pathQ){
                        PBPoint.Builder pReq = PBPoint.newBuilder();
                        pReq.setX(p.x);
                        pReq.setY(p.y);
                        req.addPoints(pReq.build());
                    }
                    robot.sendMsgToServer("C05", req.build());
                }
            }
        }else{
            Point currentP = pathQ.remove();
            this.x = currentP.x;
            this.y = currentP.y;
            PBPoint.Builder pReq = PBPoint.newBuilder();
            pReq.setX(currentP.x);
            pReq.setY(currentP.y);
            robot.sendMsgToServer("C02", pReq.build());
        }
    }
    
    @Router(type="C03", msgType=StopMoveRes.class)
    public void roleStop(StopMoveRes msg){
        if(msg.getId() == robot.getGid()){
            x = msg.getPoint().getX();
            y = msg.getPoint().getY();
        }
    }
    private boolean walkable(int x, int y){
        try{
            return Room.mapPointTbl.get(map)[y][x] >= 1;
        }catch(IndexOutOfBoundsException ex){
            return false;
        }
    }
}
