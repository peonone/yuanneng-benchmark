/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sstars.yuanneng.benchmark.robot.features;

import com.google.protobuf.Message;
import com.sstars.yuanneng.benchmark.meta.Router;
import com.sstars.yuanneng.benchmark.robot.Robot;
import com.sstars.yuanneng.benchmark.utils.StringUtils;
import java.util.Date;
import java.util.Random;
import proto.login.Login.ConnReq;
import proto.login.Login.CreateReq;
import proto.login.Login.EnterMapRes;
import proto.login.Login.ReconnReq;
import proto.login.Login.ReconnRes;
import proto.login.Login.FlagReq;

/**
 *
 * @author madk
 */
public class Login extends Feature{
    private static final Random random = new Random(new Date().getTime());
    private static final String MD5_KEY = "b7fe06bb4435d8ba21431215963799cd";
    
    private ReconnReq reconnReq;
    private int curMap;
    private long startTime;
    public Login(Robot robot){
        super(robot);
    }
    
    private String getAccount(){
        return String.format("robot_%s", robot.getNum());
    }
    public void login(){
        if(reconnReq != null) {
            robot.sendMsgToServer("A04", reconnReq);
        }else {
            ConnReq.Builder req = ConnReq.newBuilder();
            String account = getAccount();
            int sid = 0;
            int ts = (int) (new Date().getTime() / 1000);
            String md5Str = StringUtils.md5String(String.format("%s%d%d%s",account,sid,ts,MD5_KEY ));
            req.setAccount(account);
            req.setSid(sid);
            req.setFcm("1");
            req.setTimestamp(ts);
            req.setChecksum(md5Str);
            startTime = new Date().getTime();
            robot.sendMsgToServer("A01", req.build());
        }
    }
    
    @Router(type = "A01")
    public void loginOk(Message msg){
        robot.sendMsgToServer("A03");
    }
    
    @Router(type = "A01", flag=-1, msgType = FlagReq.class)
    public void noRole(Message msg){
        System.out.println(String.format("第%d号机器人没有角色，正在创建", robot.getNum()));
        CreateReq.Builder req = CreateReq.newBuilder();
        req.setNick(String.format("机器人%d", robot.getNum()));
        req.setPro(random.nextInt(3) + 1);
        req.setGender(random.nextInt(2));
        robot.sendMsgToServer("A02", req.build());
    }
    
    @Router(type = "A02")
    public void createOk(Message msg){
        robot.sendMsgToServer("A03");
    }
    
    @Router(type = "A04")
    public void reconnectOk(Message msg){
        robot.sendMsgToServer("A07");
    }
    
    @Router(type = "A05", msgType=ReconnRes.class)
    public void reconnect(ReconnRes msg){
        ReconnReq.Builder req = ReconnReq.newBuilder();
        req.setType(msg.getType());
        req.setChecksum(msg.getChecksum());
        req.setAccount(getAccount());
        req.setSid(0);
        reconnReq = req.build();
        curMap = msg.getMap();
        robot.connectOtherPort(msg.getIp(), msg.getPort());
    }
    
    private boolean firstEnerMap  = true;
    @Router(type = "A07", msgType=EnterMapRes.class)
    public void enteredMap(EnterMapRes msg){
        if(firstEnerMap) {
            System.out.format("第%d号机器人进入地图，用时:%dms\n",robot.getNum(),(int)(new Date().getTime() - startTime));
            Robot.newEntered();
        }
        if(!robot.isEnteredGame()){
            robot.enteredGame(msg.getUserid(),curMap,msg.getX(),msg.getY());
        }
    }
}
