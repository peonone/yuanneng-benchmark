/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sstars.yuanneng.benchmark;

import com.sstars.yuanneng.benchmark.robot.Robot;
import java.nio.channels.Channel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author madk
 */
public class Main {
    public static Selector selector;
    private static Map<Channel, Robot> robotMapping = new HashMap<Channel, Robot>();
    
    public static void registerRobot(Channel channel, Robot robot) {
        robotMapping.put(channel, robot);
    }
    
    public static void unregisterRobot(Channel channel) {
        robotMapping.remove(channel);
    }
    
    public static void main(String[] args) throws Exception{
        parseArgs(args);
        try{
            selector = Selector.open();
        }catch(Exception ex){
            System.err.format("open selector failed\n");
            return;
        }
        
        for(int i = 1; i <= Setting.getCount(); ++i){
            Robot robot = new Robot(i + Setting.getStartNo());
        }
        while(true) {
            for(Robot robot : robotMapping.values()) {
                if(robot.isEnteredGame()) {
                    robot.doSomeThing();
                }
            }
            selector.select(200);
//            if(selector.select(200) == 0) {
////                continue;
//            }
            for(SelectionKey key : selector.selectedKeys()){
//                if(key.isConnectable()) {
//                    if(((SocketChannel)key.channel()).finishConnect()) {
//                        robotMapping.get(key.channel()).connected();
//                    }
//                }
                if(key.isReadable()){
                    robotMapping.get(key.channel()).readMsg();
                }
            }
            
        }
    }
    
    private static void parseArgs(String[] args){
        Setting.readConfigFiles();
        gnu.getopt.Getopt getopt = new gnu.getopt.Getopt("yuanneng-benchmark", args, "h:p:c:");
        int opt;
        while((opt = getopt.getopt()) != -1){
            switch(opt){
                case 'h':
                    Setting.setHost(getopt.getOptarg());
                    break;
                case 'p':
                    Setting.setPort(Integer.parseInt(getopt.getOptarg()));
                    break;
                case 'c':
                    Setting.setCount(Integer.parseInt(getopt.getOptarg()));
            }
        }
        Setting.checkAndPrintCfg();
    }
}
