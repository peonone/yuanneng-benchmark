/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sstars.yuanneng.benchmark;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

/**
 *
 * 各项配置数据的管理
 * @author madk
 */
public class Setting {
    private static String host;
    private static int port;
    private static int count;
    private static int maxTry = 3;
    private static int startNo = 0;
    private static boolean openChat = false;


    
    /**
     * 读取配置文件
     */
    public static void readConfigFiles(){
        Properties prop = new Properties();
        try{
            InputStream is = new FileInputStream("yuanneng-benchmark.cfg");
            prop.load(is);
            host = prop.getProperty("host");
            try{
                port = Integer.parseInt(prop.getProperty("port"));
            }catch(Exception ex){}
            try{
                count = Integer.parseInt(prop.getProperty("count"));
            }catch(Exception ex){}
            try{
                openChat = Integer.parseInt(prop.getProperty("open_chat")) > 0;
            }catch(Exception ex){}
            try{
                startNo = Integer.parseInt(prop.getProperty("start_no"));
            }catch(Exception ex){}
        }catch(Exception ex){
            System.err.println("read config file failed");
        }
    }
    
    public static void checkAndPrintCfg() {
        if (host == null){
            System.err.println("host(连接的服务器地址)参数不能为空，请在配置文件或命令行中指定");
            System.exit(1);
        }
        if(port == 0){
            System.err.println("port(连接的服务器端口)不能为空，请在配置文件或命令行中指定");
            System.exit(2);
        }
        if(count == 0){
            System.err.println("count(发起的连接数目)不能为空，请在配置文件或命令行中指定");
            System.exit(3);
        }
        System.out.println(String.format("服务器: %s:%d,连接数量:%d", host,port,count));
    }

    public static String getHost() {
        return host;
    }

    public static void setHost(String host) {
        Setting.host = host;
    }

    public static int getPort() {
        return port;
    }

    public static void setPort(int port) {
        Setting.port = port;
    }

    public static int getCount() {
        return count;
    }

    public static void setCount(int count) {
        Setting.count = count;
    }
    
    public static int getMaxTry() {
        return maxTry;
    }

    public static void setMaxTry(int maxTry) {
        Setting.maxTry = maxTry;
    }
    public static boolean isOpenChat() {
        return openChat;
    }
    public static int getStartNo() {
        return startNo;
    }
}
