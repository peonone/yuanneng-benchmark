/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sstars.yuanneng.benchmark.utils;

import java.math.BigInteger;
import java.security.MessageDigest;

/**
 *
 * @author madk
 */
public class StringUtils {
    
    public static String md5String(String src){
        try{
            MessageDigest md5 = MessageDigest.getInstance("md5");
            md5.reset();
            md5.update(src.getBytes());
            String signature = new BigInteger(1,md5.digest()).toString(16);
            return signature;
        }catch(Exception ex){
            return "";
        }
    }
}
